<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // seeds 5 likes for "Yalnızım Ben" album
        foreach ((range(1, 5)) as $index) {
            DB::table('likes')->insert(
                [
                    'likeable_id' => 1,
                    'likeable_type' => 'App\Album',
                ]
            );
        }

        // seeds 20 likes for "Mançoloji" album
        foreach ((range(1, 20)) as $index) {
            DB::table('likes')->insert(
                [
                    'likeable_id' => 2,
                    'likeable_type' => 'App\Album',
                ]
            );
        }

        // seeds 3 likes for "Sorma" song
        foreach ((range(1, 3)) as $index) {
            DB::table('likes')->insert(
                [
                    'likeable_id' => 1,
                    'likeable_type' => 'App\Song',
                ]
            );
        }

        // seeds 2 likes for "Haydar Haydar" song
        foreach ((range(1, 2)) as $index) {
            DB::table('likes')->insert(
                [
                    'likeable_id' => 2,
                    'likeable_type' => 'App\Song',
                ]
            );
        }

        // seeds 10 likes for "İnanmazsın" song
        foreach ((range(1, 10)) as $index) {
            DB::table('likes')->insert(
                [
                    'likeable_id' => 3,
                    'likeable_type' => 'App\Song',
                ]
            );
        }

        // seeds random like for songs(assume that there at least 9 songs)
        foreach ((range(1, 50)) as $index) {
            DB::table('likes')->insert(
                [
                    'likeable_id' => rand(1,9),
                    'likeable_type' => 'App\Song',
                ]
            );
        }

        // seeds random like for albums(assume that there at least 2 songs)
        foreach ((range(1, 10)) as $index) {
            DB::table('likes')->insert(
                [
                    'likeable_id' => rand(1,2),
                    'likeable_type' => 'App\Album',
                ]
            );
        }
    }
}
