<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Mustafa Çelik',
            'email' => 'celikmustafa89@gmail.com',
            'password' => bcrypt('123123'),
        ]);
        DB::table('users')->insert([
            'name' => 'Yöresel Kelime',
            'email' => 'yoreselkelimeler@gmail.com',
            'password' => bcrypt('123456'),
        ]);
    }
}
