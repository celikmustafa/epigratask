<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1.user admin
        DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 1,
        ]);
        // 1.user user
        DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 2,
        ]);
        // 2.user admin
        DB::table('role_user')->insert([
            'user_id' => 2,
            'role_id' => 1,
        ]);
        // 2.user user
        DB::table('role_user')->insert([
            'user_id' => 2,
            'role_id' => 2,
        ]);
    }
}
