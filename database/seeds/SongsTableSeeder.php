<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SongsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('songs')->insert([
            'title' => 'Sorma',
            'album_id' => 1,
        ]);
        DB::table('songs')->insert([
            'title' => 'Haydar Haydar',
            'album_id' => 1,
        ]);
        DB::table('songs')->insert([
            'title' => 'İnanmazsın',
            'album_id' => 1,
        ]);
        DB::table('songs')->insert([
            'title' => 'Asla Bırakma',
            'album_id' => 1,
        ]);
        DB::table('songs')->insert([
            'title' => 'Rüzgar',
            'album_id' => 1,
        ]);
        DB::table('songs')->insert([
            'title' => 'Anlıyorsun Değil Mi',
            'album_id' => 2,
        ]);
        DB::table('songs')->insert([
            'title' => 'Dönence',
            'album_id' => 2,
        ]);
        DB::table('songs')->insert([
            'title' => 'Halil İbrahim Sofrası',
            'album_id' => 2,
        ]);
        DB::table('songs')->insert([
            'title' => 'Domates Biber Patlıcan',
            'album_id' => 2,
        ]);
    }
}
