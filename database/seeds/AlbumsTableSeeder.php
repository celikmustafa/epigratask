<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlbumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('albums')->insert([
            'name' => 'Yalnızım Ben',
            'singer' => 'Can Gox',
        ]);
        DB::table('albums')->insert([
            'name' => 'Mançoloji 2',
            'singer' => 'Barış Manço',
        ]);
    }
}
