<?php

namespace App\Listeners\Register;

use App\Events\Register\UserRegisteredEvent;
use App\Notifications\NewUserNotificationEmailToAdmin;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdministrator
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegisteredEvent  $event
     * @return void
     */
    public function handle(UserRegisteredEvent $event)
    {
        //send notification to administrators
        //dd($event->user->getAttribute('name'));
        //$admin = User::where('id', 2)->first();
        $admins = Role::where('name','admin')->first()->users()->get();

        //dd($admins);
        //TODO: multiple email gönderme durumu detaylı araştır
        $count = 0;
        $emails = '';
        foreach ($admins as $admin) {
            $admin->notify(new NewUserNotificationEmailToAdmin($event->user));
            $count = $count + 1;
            $emails = array($emails, $admin->email) ;
            //Notification::route('mail',$admin->email)->notify(new NewUserNotificationEmailToAdmin($event->user));
        }
    }
}
