<?php

namespace App\Http\Controllers;

use App\Album;
use App\Song;
use Illuminate\Http\Request;
use function Sodium\add;

class PolymorphicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::orderBy('name', 'asc')->get();
        $albumLikes = array();
        $songLikes = array();
        foreach ($albums as $album){
            $count = Album::find($album->id)->likes()->count();
            $albumLikes = array_add($albumLikes,$album->id,$count);
        }
        $songs = Song::orderBy('title', 'asc')->get();
        foreach ($songs as $song){
            $count = Song::find($song->id)->likes()->count();
            $songLikes = array_add($songLikes,$song->id,$count);
        }

        return view('polymorphic',compact('albums', 'songs','albumLikes', 'songLikes'));
    }
}
