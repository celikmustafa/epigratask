<?php

namespace App\Http\Controllers\Auth;

use App\Events\Register\UserRegisteredEvent;
use App\Mail\Welcome;
use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // creates user
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        // gets the 'user' role if not exists creates the role.
        $role = Role::where('name', 'user')->first();
        if($role == null){
            $role = Role::create([
                'name' => 'user',
            ]);
        }

        // if user and role both exists runs the code
        if($role != null && $user != null){
            // creates pivot record between user-role
            $user->roles()->attach($role);

            // emails user for the registration
            Mail::to($user)->send(new Welcome($user));

            // throws an alert for the user
            session()->flash('message','Aktivasyon emailiniz gönderilmiştir...');

            // notifies the listeners for registration
            UserRegisteredEvent::broadcast($user);
        }

        return $user;
    }
}
