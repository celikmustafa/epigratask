@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Polymorphic Relation Örneği</h1>
                <script language="javascript">
                    function toggle() {
                        var ele = document.getElementById("toggleText");
                        var text = document.getElementById("displayText");
                        if(ele.style.display == "block") {
                            ele.style.display = "none";
                            text.innerHTML = "Açıklamayı Göster";
                        }
                        else {
                            ele.style.display = "block";
                            text.innerHTML = "Açıklamayı Gizle";
                        }
                    }
                </script>
                <a id="displayText" onclick="toggle()">Açıklamayı Gizle</a>
                <div id="toggleText" class="alert alert-info">
                    <h3 style="margin-top: 0">Açıklama</h3>
                    Bu sayfa <strong>polymorphic relation</strong> örneği için hazırlanmıştır. <br>
                    Senaryo gereği <strong>album</strong>, <strong>song</strong> ve <strong>like</strong> olmak üzere 3 tablomuz var.<br>
                    <strong>Album</strong> ve <strong>Song</strong> farklı tablolar olmasına rağmen, ikisinin de <strong>Like</strong> tablosu ile ilişkisi var. <br>
                    Yani hem albüm like alabilir, hem de song. <br>
                    Bu durum da Like tablosu, Song ve Album ile <strong>polymorphic</strong> ilişki içinde.<br>
                    <strong><a href="https://i.hizliresim.com/ROMmQR.jpg">Tabloların diagramını görmek için buraya tıklayınız...</a></strong>
                </div>



                <div class="panel panel-primary">

                    <div class="panel-heading text-center">Albümler</div>

                    <div class="panel-body">
                        @foreach($albums as $album)

                            <div class="panel panel-success">
                                <div class="panel-heading ">
                                    {{$album->name}} - {{$album->singer}}
                                    <span class="badge"> {{$albumLikes[$album->id]}}</span>
                                </div>

                                @foreach($songs as $song)
                                    @if($song->album_id == $album->id)
                                        <li class="list-group-item">
                                            <span>{{$song->title}}</span>
                                            <span class="badge">{{$songLikes[$song->id]}}</span>
                                        </li>
                                    @endif
                                @endforeach


                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
