@component('mail::message')
# {{ env('APP_NAME') }} ailesine hoş geldin {{ $user->name }},

Uygulama kaydın başarı ile gerçekleşmiştir.

@component('mail::button', ['url' => 'http://epigra-task.herokuapp.com/home'])
Sayfaya Git
@endcomponent

Teşekkürler, <br>
{{ env('APP_NAME') }}
@endcomponent
