@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if($flash = session('message'))
                <div class="alert alert-success" role="alert">
                    {{$flash}}
                </div>
            @endif
            <h2>Uygulama Tanıtım Videosu</h2>
                <iframe width="560" height="315"
                        src="https://www.youtube.com/embed/rQL01SvmUHw?rel=0&amp;showinfo=0"
                        frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                
            <h2>Source File</h2>
                <a href="https://bitbucket.org/celikmustafa/epigratask">Bitbucket Repo</a>

        </div>
    </div>
</div>
@endsection
