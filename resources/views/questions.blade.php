@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <h1>Teknik Sorular</h1>

            <div class="alert alert-info">

                <h4><strong>1. Laravel üzerinde custom middleware kullanımı ile ilgili bilgi ve örnek bir senaryo verebilir misin?</strong></h4>

                <p>
                    Middleware, uygulamaya gelen veya giden(terminable middleware) HTTP isteklerinin pratik bir şekilde filtrelenmesini sağlayan mekanizmadır.
                    Verilebilecek en temel örnek <strong>kimlik doğrulama</strong> filtresidir.
                    Eğer kullanıcı kimlik doğrulaması yapmadan uygulamaya erişmek isterse, Authentication filtresine(middleware) takılıp login sayfasına yönlendirilir.
                    Yine benzer şekilde kimliği doğrulanmış şekilde geliyorsa, aynı filtreden istediği alana erişimine izin verilir.
                </p>
                <p>
                    Ama anladığım kadarıyla <strong>Auth</strong> ve <strong>CsrfToken</strong> gibi bazı middleware'ler Laravel içerisinde <strong>built-in</strong> olarak mevcut.
                    Bunlar haricinde uygulamaya özgü bir middleware'e ihtiyaç duyulduğunda <strong>custom</strong> bir middleware da yazılabilir.
                    Yine ihtiyaca göre, bir middleware'ı <strong>global</strong> yaparak gelen tüm istekler bu filtreden geçirilebildiği gibi; belli <strong>route</strong>'lara özel middleware da atanabilir.
                    <strong>Terminable</strong> middleware ile de istemciye response gönderdikten sonra işlem yapılabilir.
                    Middleware Gruplama ve parametreli kullanımları da, pratiklik açısından ve ihtiyaca yönelik kullanılabilir.
                </p>
                <p>
                    <strong>Örnek:</strong> birden fazla dil seçeneği olan bir uygulama düşünelim(<strong>udemy.com</strong>).
                    Çeşitli dillerde online eğitim verme ve eğitim alma imkanı tanıyor.
                    Yani siteyi Türkiyeden açan birinin Türkçe, Almanyadan açan birinin Almanca görmesi kullanıcı açısından çok kullanışlı olur.
                    Bu işlemi yapmak için, bir <strong>lokasyon</strong> ya da <strong>dil</strong> middleware'ı yazılıp, gelen isteğin lokasyonuna bakarak orada konuşulan dile göre sayfanın o dilde gösterilmesi güzel bir pratik olur.
                </p>

            </div>

            <div class="alert alert-info">

                <h4><strong>2. </strong>User::get()->where('id',1)->pluck()<strong> ile </strong>User::where('id',1)->pluck()<strong> arasındaki farkı açıklayabilir misin?</strong></h4>
                <p>
                    Bu soruyu çalıştırdığım sorgulardan anladığım kadarıyla cevaplayım. <br>
                    <strong>Birinci</strong> de, tüm kullanıcılar çekiliyor ve çekilen kullanıcılar arasından id'si 1 olan kullanıcının, pluck içerisinde verilen sütun(artık hangisi olursa o kısım) alınıyor.
                </p>
                <p>
                    <strong>İkinci</strong> de, id'si 1 olan kullanıcının pluck içerisinde istenen sütunları çekiliyor.
                </p>
                <p>
                    <strong>Sonuç olarak</strong> aynı output'u verirler.
                    Ama anladığım kadarıyla, birincisi daha fazla işlem yapıyor.
                    Önce tüm kullanıcıları çekiyor,sonra bunlar arasında id'si 1 olanı çekiyor.
                    (Gereksiz bir işlem gibi geldi.)
                </p>



            </div>
            <div class="alert alert-info">

                <h4><strong>3. Migration ve Seeder'ın farklarını ve kullanım alanlarını açıklayabilir misin?</strong></h4>
                <p>
                    <strong>migration:</strong> database şemalarının oluşturulması, düzenlenmesi ve paylaşılması için kullanılıyor.
                    Birden fazla geliştiricinin çalıştığı bir projede, database üzerinde yapılan değişikliklerin tüm kullanıcılar tarafından kolaylıkla kullanılması sağlanıyor.
                    Özetle database şema oluşturma işlemi için tasarlanmış bir <strong>Git</strong> diyebiliriz.
                </p>
                <p>
                    <strong>seeder:</strong> migration ile oluşturulan db içerisinde herhangi bir kayıt bulunmaz.
                    Sadece tablolar oluşturulur. Seeder ile, uygulamaya test aşaması için gereken <strong>test verileri</strong> eklenir.
                    Herkesin aynı test verileri üzerinde çalışması açısından da bir Git işlevi görmüş olur.
                </p>
                <p>
                    <strong>Temel farkları</strong>, migration database şeması oluşturur, seeder oluşan şemalara veri girişi yapar.
                </p>
                <br>
                <strong>Not:</strong> Verilen task'in polymorphic relation ile alakalı kısmında <strong>migration</strong> ile tablo oluşturma ve <strong>Seeder</strong> ile test verisi ekleme işlemleri kullanıldı.

            </div>
            <div class="alert alert-info">

                <h4><strong>4. Laravel Facade'ları nasıl çalışmakta ve neyi sağlamaktadır?</strong></h4>
                <p>Laravel içerisinde, routing, caching, database operasyonları v.b için gerekli methodlar, aslında <strong>Service Container'</strong>ı içerisindeki sınıflarda tutuluyormuş.
                Laravel'in ihtiyaç duyduğu bu methodları service container içerisinden direk alması okunabilirlik, test ve esneklik açısından problem oluşturmaktaymış.
                <br> Bu karmaşayı ortandan kaldırmak için service container'ı önünde çalışan bir <strong>static proxy(static interface)</strong> oluşturulmuş.
                Mesela db işlemleri için service container'a, <strong>Schema::</strong> facade'ı üzerinden ulaşmaktaymış. Benzer şekilde <strong>Route::, Mail::, Cache::</strong> vs.
                <br>Böylece <strong>facade</strong>'lar kullanılarak, laravel feature'ları <strong>kısa</strong>, <strong>öz</strong> ve <strong>anlaşılabilir</strong> bir <strong>syntax</strong> sunulmuş.
                Ayrıca bu şekilde <strong>esneklik</strong> ve <strong>test</strong> işlemleri daha kolay hale gelmiş.
                Bir de Laravel Facade'ları ile design pattern facade aynı şey değilmiş.
                </p>



            </div>
            <div class="alert alert-info">

                <h4><strong>5. compact fonksiyonu ne işe yarar?</strong></h4>
                <strong>php dokümantasyonuna göre:</strong> değişkenleri ve değişkenlerin değerlerini içerisinde tutan bir array oluşturuyormuş. Kullandığım kadarıyla array oluşturmak için <strong>pratik</strong> bir yöntem.
                <br>
                <strong>örnek kod:</strong>
                <p style="background-color: white">
                    // aşağıdaki 3. ve 4. kod satırlarında <br>
                    // <strong>['name' => 'mustafa', 'surname' => 'celik']</strong> array'i oluşturuluyor.<br>
                    // burda görüldüğü üzere, compact() kullanımı daha <strong>pratik</strong>.<br>
                    1. $name = 'mustafa';<br>
                    2. $surname = 'celik';<br><br>

                    3. return view('welcome', compact('name', 'surname'));<br>
                    4. return view('welcome',['name' => $name, 'surname' => $surname]);<br>
                </p>



            </div>
            <div class="alert alert-info">

                <h4><strong>6. Trait kullanımına bir örnek verebilir misin?</strong></h4>
                <p>
                    <strong>Trait</strong>, kod tekrarının olduğu durumlarda tercih ediliyor.
                    <br><strong>Örneğin</strong>, verilen bir metinin, toplam karakter sayısının hesaplanması gereken bir durumda;
                    bu işlemi yapan bir metod trait altında yazılabilir.
                    Uygulama için böyle bir işleme ihtiyaç duyan her alan bu trait'i kullanarak karakter sayısını bulabilir.
                </p>



            </div>
            <div class="alert alert-info">

                <h4><strong>7. PHP require ve include fonksiyonlarının arasındaki fark nedir?</strong></h4>
                <p>
                    <strong>include</strong> kullanılan kod satırına gelindiğinde ve aranılan dosya bulanamadığında <strong>warning</strong> verilir ve script çalışmaya <strong>devam eder</strong>.
                    <br>
                    <strong>require</strong> satırana gelindiğinde ve dosya bulunamadığında <strong>fatal_error</strong> verir ve script <strong>durur</strong>.
                    <br>
                    <strong>özetle</strong> olmazsa olmaz dosyaların gerektiği yerde require kullanılır.
                </p>



            </div>
            <div class="alert alert-info">

                <h4><strong>8. PHP'deki hata türleri kaç çeşittir ve nelerdir?</strong></h4>
                <strong>4</strong> farklı hata başlığında toplanmıştır.
                <strong>warnings</strong> ve <strong>fatal_errors</strong> ana başlıkları da;
                    <strong>startup</strong>, <strong>compile_time</strong> ve <strong>run_time</strong> olmak üzere <strong>3</strong> alt başlığa ayrılmış.
                <br><br><strong>1.notices:</strong> non-critical errors
                <br><strong>2.warnings:</strong> include() fonksiyonun dosya bulamayıp hata vermesi gibi. script çalışmayı sürdürüyor ama ortada bir hata var.
                <br><strong>3.parse_error:</strong> syntax error, kod compilation sırasında meydana gelen hatalar.
                <br><strong>4.fatal_errors:</strong> require() örneği gibi, scripting çalışmayı tamamen durması ile sonuçlanan hatalar
                <br><br>
                Buraya da bazı hata örnekleri koyuyorum ama tecrübelerime dayalı değil de internetten bulduğum örnekler.
                <br><strong>E_STRICT:</strong> Run-time notices.
                <br><strong>E_NOTICE:</strong> Run time notice caused due to error in code
                <br><strong>E_USER_NOTICE:</strong> User-generated notice message.
                <br><strong>E_WARNING:</strong> Run-time warning that does not cause script termination
                <br><strong>E_CORE_WARNING:</strong> Warnings that occur during PHP’s initial startup
                <br><strong>E_USER_WARNING:</strong> User-generated warning message.
                <br><strong>E_ERROR:</strong> A fatal error that causes script termination
                <br><strong>E_CORE_ERROR:</strong> Fatal errors that occur during PHP’s initial startup (installation)
                <br><strong>E_PARSE:</strong> Compile time parse error.
                <br><strong>E_COMPILE_ERROR:</strong> Fatal compile-time errors indication problem with script.
                <br><strong>E_USER_ERROR:</strong> User-generated error message.
                <br><strong>E_RECOVERABLE_ERROR:</strong> Catchable fatal error indicating a dangerous error
                <br><strong>E_ALL:</strong> Catches all errors and warnings



            </div>
            <div class="alert alert-info">

                <h4><strong>9. PHP isset ve empty fonksiyonları arasındaki fark nedir?</strong></h4>
                php manual'daki tanımlarında:
                <br><strong>isset-></strong> determine if a variable is set and is not NULL
                <br><strong>empty-></strong> determine whether a variable is empty
                <br>
                <br>
                özetle bir değişkene herhangi bir değer atanmadığında ve direk NULL değeri atandığında <strong>isset()</strong> fonksiyonu <strong>FALSE</strong> döner, diğer durumlar <strong>TRUE</strong> döner.
                <br><strong>empty()</strong> fonksiyonu da <strong>"", False, "0", 0, 0.0 ve NULL</strong> değerlerini boş olarak kabul eder ve <strong>TRUE</strong> döner.



            </div>
            <div class="alert alert-info">

                <h4><strong>10. Warning: Cannot modify header information – headers already sent hatası alınıyorsa bunun ilk olası sebebi ne olabilir?</strong></h4>
                <p>
                    Verdiğiniz task'i yaparken bu hata ile karşılaşmadım. <br>
                    Hata mesajını simule etmeye çalıştım ama alamadım bu hatayı :) <br>
                    Stackoverflow'da sağlam RP alan bir yorumdan anladığım kadarıyla bahsedeyim: <br>
                    PHP genelde, <strong>HTML içerik</strong> üretip webserver'a gönderiyormuş, ama <strong>HTTP header</strong> kısmına eklemeler de yapıyormuş.<br>
                    Çalışırken öncelikle, isteğin <strong>HTTP header</strong> bilgilerini, daha sonra arada 2-satır boşluk bırakıp <strong>html içeriği</strong> gönderiyormuş.<br>
                    Header alanı geçildikten sonra buraya tekrar müdahale edemiyormuş, ekleme yapamıyormuş, yapmamalıymış. <br>
                    Anladığım kadarıyla böyle bir çabaya girildiğinde bu hata ile karşılaşılıyor. <br>
                    Birden fazla sebepten bahsedilmiş ama forumlardan anladığım kadarıyla: <br>
                    Dosyanın <strong>UTF8 without BOM</strong> formatında olmaması ve <strong>< ?php</strong> ifadesinden önce whitespace kullanılması öncelikli nedenler. <br>
                </p>



            </div>
            <div class="alert alert-info">

                <h4><strong>11.</strong></h4>
                <p style="background-color: white"><strong>1. var_dump(0123 == 123); //bool(false)</strong>
                    <br><strong>2. var_dump('0123' == 123); //bool(true)</strong>
                    <br><strong>3. var_dump('0123' === 123);  //bool(false)</strong></p>
                <strong>çıktıları ne olur? Nedenleriyle açıklayabilir misin?</strong>
                <br>
                <br>

                öncelikle,
                <br><strong>== (equal):</strong> değişkenlerin değerlerini kıyaslar, bunlar farklı tiplerde(string==integer) olabilir. Ama aynı değere sahipse true döner.
                <br><strong>=== (identical):</strong> değişkenlerin hem değerlerini hem de tiplerini kıyaslar. yani true dönmesi için hem değerler eşit hem de veri tipleri aynı olmalıdır.
                <br>
                <br>
                sadece yukardaki açıklamaya bakarak <strong>3.</strong> satırdaki kod <strong>bool(false)</strong> döner çünkü veri tipleri farklıdır (string===integer)
                <br>
                <br>
                bir de, php manual'de geçen açıklamaya göre numeric yani bir sayı ile bir string equal ile kıyaslandığında string numberic cinsine çevrilerek kıyaslanırmış.
                <br>bu bilgiye de dayanarak <strong>2.</strong> satırdaki kod <strong>bool(true)</strong> değerini verir çünkü '0123' string değeri 123'e çevrilerek (123==123) şeklinde kıyaslaması yapılır.
                <br>
                <br>
                yine manual'e göre integer bir sayı başında <strong>0</strong> ile gösteriliyorsa bu sayı <strong>8'lik sistem</strong> ile gösteriliyor demektir.
                <br>Bu bilgiye de dayanarak <strong>1.</strong> satırdaki kod <strong>bool(false)</strong> değerini verir çünkü 0123 sayısının 10'luk sistem karşılığı 83 tür.
                Yani (83==123) kıyaslaması yapılmaktadır.



            </div>
            <div class="alert alert-info">

                <h4><strong>12. === ve == kullanımı arasındaki fark nedir?</strong></h4>
                <strong>== (equal):</strong> değişkenlerin değerlerini kıyaslar. bunlar farklı tiplerde(string==integer) olsa bile kıyaslamayı yapar ve eşitlik varsa true döner.
                <br><strong>=== (identical):</strong> değişkenlerin hem değerlerini hem de tiplerini kıyaslar. yani true dönmesi için hem değerler eşit hem de veri tipleri aynı olmalıdır.



            </div>
            <div class="alert alert-info">

                <h4><strong>13.Her hangi loop veya if else koşulu kullanmadan bir sayının tek sayı olduğunu yazdırabilir misin?</strong></h4>
                <p style="background-color: white">
                    $number = rand();
                    <br>$text = ['çift','tek'];
                    <br>echo $number;
                    <br>echo $text[$number%2];
                </p>



            </div>
            <div class="alert alert-info">

                <h4><strong>14.PSR ve faydaları nelerdir? Hangilerini uyguluyorsun?</strong></h4>
                <p>
                    Sanırım bu soru kodlama standartları konusundaki bilgileri ölçmek için.
                    Daha önce php kullanmadığım için PSR duymadım ama benzer şeyleri diğer programlama dillerinde kullandım.
                    Bu şekilde standartlara tam anlamıyla uyarak kullandığım dil python'dı.
                    İş hayatına yeni girdiğimde bana verilen ilk iş tüm ekibin uymak zorunda olduğu python standartlarının dokümantasyonun hazırlanmasıydı.
                    Bu dokümantasyonu hazırlarken standartlar konusunda güzel tecrübelerim oldu.
                    Ayrıca python bu konuda çok katı olduğu ve openstack community'e de bir kaç commit gönderdiğim için standartların ekipler için ne kadar önemli olduğunu gördüm.
                    Ayrıca IDEA üzerinde ve commit öncesi kodlama standart hatalarını gösteren eklentiler de kullandım.
                </p>
                <p>
                    Ama okuduğum kadarıyla php bu konuda zamanında baya gevşek davrandığı için farklı farklı third-party kütüphane kullanımlarında büyük sorunlar olmuş.
                    Sanıyorum bunun gibi büyük sorunları engellemek için <strong>PSR-0/PSR-4</strong> uygulamak zorunlu.
                    Ayrıca logging, http message, temel kod standartlarını merkezileştirmek için diğer PSR standartlarınada uymak gerekiyor.
                </p>
                <p>
                    Dile özgü ufak tefek standart farklılık var anladığım kadarıyla mesela "metod açılış kapanış parantezi alt satırda olmalı" diyor.
                    Java'da sadece kapanış alt satırda, pythonda parantez olayı hiç yok. Dile göre farklılıklar haricinde genel olarak standartlara aşinalığım var.
                </p>



            </div>
            <div class="alert alert-info">

                <h4><strong>15.</strong></h4>
                <p style="background-color: white">
                    $x = true and false;
                    <br>var_dump($x);
                    </p>
                çıktısını açıklayabilir misin?
                <br><br>
                cevap <strong>bool(true)</strong> döner.
                <br>php manual'e göre <strong>'and'</strong> operatörünün işlem önceliği yokmuş.
                işlem önceliği olmadığı için <strong>=</strong> operator işlemini önce uyguluyor.
                sonuçta şu şekilde bir işlem gerçekleşiyor -> <strong>($x = true) and false;</strong>
                <br>
                Bu da <strong>$x</strong> değişkenine <strong>true</strong> değeri verdiğinden, sonuç <strong>true</strong> oluyor.
                Bu hataya düşmemek için <strong>'and'</strong> ve <strong>'or'</strong> operatorleri kullanırken mutlaka parantez kullanılmalı.
                Veya hiç parantez durumlarına bakılmadan işlem önceliği olan <strong>'&&'</strong> ve <strong>'||'</strong> operatorler tercih edilmelidir.



            </div>
            <div class="alert alert-info">

                <h4><strong>16. Twitter gibi bir sistem yazacak olsaydın hangi sınıfları kullanırdın?</strong></h4>
                Burda sınıftan kastı, database tablolarına karşılık gelen sınıflar olarak anladım.
                <br>Bu durumda:
                <br>users -> kullanıcılar tutulur
                <br>tweets -> tweet'ler tutulur
                <br>hashtags -> hashtag'ler tutulur
                <br>user_tweet -> user tweet bağlantısı için(tweet, retweet ve like olarak 3 tip ilişki olabilir)
                <br>tweet_hashtag -> tweet eğer bir veya birden fazla hastag'e ait ise
                <br>tweet_response -> bir tweet'e gönderilen response'lar tutulur
                <br>user_following -> kullanıcıların takip ettikleri kişiler tutulur.(takip edenler de burdan çekilebilir)
                <br>images -> resimler tutulur
                <br>videos -> videolar tutulur
                <br>tweet_image -> resim tweet bağlantısı tutulur
                <br>tweet_video -> video tweet bağlantısı tutulur

            </div>

        </div>
    </div>
</div>
@endsection
